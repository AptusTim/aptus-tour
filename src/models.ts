export interface HotSpot {
    pitch: number;
    yaw: number;
    type: string;
    sceneId?: string;
    URL?: string;
}

export interface Scene {
    hfov: number;
    pitch: number;
    yaw: number;
    type: string;
    panorama: string;
    hotSpots: HotSpot[];
}

export interface Config {
    autoLoad: boolean;
    compass: boolean;
    default: {
        firstScene: string;
        sceneFadeDuration: number;
    };
    scenes: {
        [sceneId: string]: Scene;
    };
}

export interface SceneInfo {
    id: string;
    hfov: number;
    pitch: number;
    yaw: number;
    panorama: string;
    hotSpots: HotSpot[];
}
