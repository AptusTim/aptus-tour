import "pannellum";
import { locations } from "./locations";
import { Config, Scene } from "./models";
import { sceneDescriptions } from "./scenes";

declare const pannellum: {
    viewer(container: string, config: Config): void;
};

const scenes: { [sceneId: string]: Scene } = {};

sceneDescriptions.forEach(info => {
    const { id, ...scene } = info;
    scenes[info.id] = {
        ...scene,
        type: "equirectangular"
    };
});

const config: Config = {
    autoLoad: true,
    compass: false,
    default: {
        firstScene: locations.parking,
        sceneFadeDuration: 1000
    },
    scenes
};

export const init = () => pannellum.viewer("container", config);
