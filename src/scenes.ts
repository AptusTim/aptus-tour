import { images } from "./images";
import { locations } from "./locations";
import { SceneInfo } from "./models";

export const sceneDescriptions: SceneInfo[] = [

    {
        id: "eetplaats",
        hfov: 180,
        pitch: 0,
        yaw: 160,
        panorama: images.eetplaats,
        hotSpots: [
            {
                pitch: 5,
                yaw: 120,
                type: "scene",
                sceneId: locations.trapboven
            }
        ]
    },
    {
        id: "keuken",
        hfov: 180,
        pitch: 3,
        yaw: -2,
        panorama: images.keuken,
        hotSpots: [
            {
                pitch: 8,
                yaw: 50,
                type: "scene",
                sceneId: locations.inkom
            }
        ]
    },
    {
        id: "trap",
        hfov: 180,
        pitch: 25,
        yaw: 174,
        panorama: images.trap,
        hotSpots: [
            {
                pitch: 35,
                yaw: 174,
                type: "scene",
                sceneId: locations.trapboven
            },
            {
                pitch: -25,
                yaw: -8,
                type: "scene",
                sceneId: locations.inkom
            }
        ]
    },
    {
        id: "eilandaqualex",
        hfov: 180,
        pitch: 0,
        yaw: 184,
        panorama: images.eilandaqualex,
        hotSpots: [
            {
                pitch: 5,
                yaw: 183,
                type: "scene",
                sceneId: locations.marvin
            },
            {
                pitch: 0,
                yaw: 2,
                type: "scene",
                sceneId: locations.trapboveneinde
            }
        ]
    },
    {
        id: "keukenboven",
        hfov: 180,
        pitch: -5,
        yaw: -5,
        panorama: images.keukenboven,
        hotSpots: [
            {
                pitch: 5,
                yaw: -5,
                type: "scene",
                sceneId: locations.trapboveneinde
            }
        ]
    },
    {
        id: "trapboven",
        hfov: 180,
        pitch: 0,
        yaw: 88,
        panorama: images.trapboven,
        hotSpots: [
            {
                pitch: 4,
                yaw: 30,
                type: "scene",
                sceneId: locations.eetplaats
            },
            {
                pitch: 4,
                yaw: 124,
                type: "scene",
                sceneId: locations.vergaderzaal
            },
            {
                pitch: 4,
                yaw: 136,
                type: "scene",
                sceneId: locations.enlab
            },
            {
                pitch: -60,
                yaw: -60,
                type: "scene",
                sceneId: locations.trap
            },
            {
                pitch: -5,
                yaw: 277,
                type: "scene",
                sceneId: locations.trapboveneinde
            },
            {
                pitch: 0,
                yaw: 220,
                type: "scene",
                sceneId: locations.eilandaqualex
            },
            {
                pitch: 0,
                yaw: -40,
                type: "scene",
                sceneId: locations.eilandkip
            }
        ]
    },
    {
        id: "eilandkip",
        hfov: 180,
        pitch: -8,
        yaw: 185,
        panorama: images.eilandkip,
        hotSpots: [
            {
                pitch: 3,
                yaw: -95,
                type: "scene",
                sceneId: locations.standingdesk
            },
            {
                pitch: 0,
                yaw: 65,
                type: "scene",
                sceneId: locations.trapboven
            },
            {
                pitch: -1,
                yaw: 102,
                type: "scene",
                sceneId: locations.trapboveneinde
            }
        ]
    },
    {
        id: "marvin",
        hfov: 180,
        pitch: -4,
        yaw: -40,
        panorama: images.marvin,
        hotSpots: [
            {
                pitch: -6,
                yaw: -43,
                type: "scene",
                sceneId: locations.eilandaqualex
            }
        ]
    },
    {
        id: "trapboveneinde",
        hfov: 180,
        pitch: 0,
        yaw: -3,
        panorama: images.trapboveneinde,
        hotSpots: [
            {
                pitch: -4,
                yaw: 7,
                type: "scene",
                sceneId: locations.trapboven
            },
            {
                pitch: 0,
                yaw: 86,
                type: "scene",
                sceneId: locations.eilandaqualex
            },
            {
                pitch: -4,
                yaw: 176,
                type: "scene",
                sceneId: locations.keukenboven
            },
            {
                pitch: -4,
                yaw: -80,
                type: "scene",
                sceneId: locations.eilandkip
            }
        ]
    },
    {
        id: "enlab",
        hfov: 180,
        pitch: 0,
        yaw: 10,
        panorama: images.enlab,
        hotSpots: [
            {
                pitch: 8,
                yaw: 153,
                type: "scene",
                sceneId: locations.trapboven
            }
        ]
    },
    {
        id: "parking",
        hfov: 180,
        pitch: 0,
        yaw: 188,
        panorama: images.parking,
        hotSpots: [
            {
                pitch: 10,
                yaw: 188,
                type: "scene",
                sceneId: locations.inkom
            }
        ]
    },
    {
        id: "vergaderzaal",
        hfov: 180,
        pitch: 0,
        yaw: 94,
        panorama: images.vergaderzaal,
        hotSpots: [
            {
                pitch: 5,
                yaw: -59,
                type: "scene",
                sceneId: locations.trapboven
            }
        ]
    },
    {
        id: "inkom",
        hfov: 180,
        pitch: 5,
        yaw: 170,
        panorama: images.inkom,
        hotSpots: [
            {
                pitch: 15,
                yaw: 172,
                type: "scene",
                sceneId: locations.trap
            },
            {
                pitch: 12,
                yaw: 48,
                type: "scene",
                sceneId: locations.keuken
            },
            {
                pitch: 10,
                yaw: -9,
                type: "scene",
                sceneId: locations.parking
            }
        ]
    },
    {
        id: "standingdesk",
        hfov: 180,
        pitch: -5,
        yaw: 185,
        panorama: images.standingdesk,
        hotSpots: [
            {
                pitch: -15,
                yaw: 165,
                type: "scene",
                sceneId: locations.eilandkip
            },
            {
                pitch: -5,
                yaw: -28,
                type: "info",
                URL: "https://www.high-five.io"
            },
            {
                pitch: -5,
                yaw: 19,
                type: "info",
                URL: "https://www.aptus.be"
            }
        ]
    }
];
