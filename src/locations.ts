export const locations = {
    eetplaats: "eetplaats",
    keuken: "keuken",
    trap: "trap",
    eilandaqualex: "eilandaqualex",
    keukenboven: "keukenboven",
    trapboven: "trapboven",
    eilandkip: "eilandkip",
    marvin: "marvin",
    trapboveneinde: "trapboveneinde",
    enlab: "enlab",
    parking: "parking",
    vergaderzaal: "vergaderzaal",
    inkom: "inkom",
    standingdesk: "standingdesk"
};
