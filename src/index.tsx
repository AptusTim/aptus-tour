import { init } from "./app";
import "./style.css";

(() => {
    if (document.getElementById("container") !== null) { return; }

    const elemDiv = document.createElement("div");
    elemDiv.id = "container";
    document.body.appendChild(elemDiv);
    init();
})();
