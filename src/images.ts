import { locations } from "./locations";

export const images = {
    [locations.eetplaats]: require("./tour/eetplaats.jpg"),
    [locations.keuken]: require("./tour/keuken.jpg"),
    [locations.trap]: require("./tour/trap.jpg"),
    [locations.eilandaqualex]: require("./tour/eilandaqualex.jpg"),
    [locations.keukenboven]: require("./tour/keukenboven.jpg"),
    [locations.trapboven]: require("./tour/trapboven.jpg"),
    [locations.eilandkip]: require("./tour/eilandkip.jpg"),
    [locations.marvin]: require("./tour/marvin.jpg"),
    [locations.trapboveneinde]: require("./tour/trapboveneinde.jpg"),
    [locations.enlab]: require("./tour/enlab.jpg"),
    [locations.parking]: require("./tour/parking.jpg"),
    [locations.vergaderzaal]: require("./tour/vergaderzaal.jpg"),
    [locations.inkom]: require("./tour/inkom.jpg"),
    [locations.standingdesk]: require("./tour/standingdesk.jpg")
};
