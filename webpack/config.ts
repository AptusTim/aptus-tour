
import * as webpackMerge from "webpack-merge";

import { baseConfig } from "./base";

import chalk from "chalk";
import { error } from "util";
import { Configuration } from "webpack";

export const ERR_NO_ENV_FLAG = chalk.red(`You must pass an --env.env flag into your build for webpack to work!`);
export const ERR_ENV_FLAG_NOT_FOUND = chalk.red(`Supplied --env.env does not match "development" or "production"`);

export interface Environment {
    env?: "development" | "production";
    port?: number;
}

// const SpeedMeasurePlugin = require("speed-measure-webpack-plugin");
// const smp = new SpeedMeasurePlugin();

const config = (env: Environment) => {

    if (!env) {
        throw new Error(ERR_NO_ENV_FLAG);
    }

    if (env.env !== "production" && env.env !== "development") {
        throw new Error(ERR_ENV_FLAG_NOT_FOUND);
    }

    // tslint:disable-next-line:non-literal-require
    const envConfig = require(`./${env.env}.ts`);
    // tslint:disable-next-line:no-unnecessary-local-variable
    const mergedConfig: Configuration = webpackMerge(baseConfig, envConfig.config(env));

    return /*smp.wrap(*/mergedConfig/*)*/;
};

export { config as default };
