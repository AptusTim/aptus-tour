import * as CaseSensitivePlugin from "case-sensitive-paths-webpack-plugin";
import * as ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import * as HtmlWebpackPlugin from "html-webpack-plugin";
import * as webpack from "webpack";
import { Configuration } from "webpack";
import { Environment } from "./config";
import { babelOptions } from "./helpers/babelOptions";
import { paths } from "./path";

const CircularDependencyPlugin = require("circular-dependency-plugin");

// tslint:disable-next-line:max-func-body-length
export const config = (env: Environment): Configuration => ({
    devtool: "eval-source-map",
    mode: "development",
    module: {
        strictExportPresence: false,
        rules: [
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.font\.js/,
                use: [
                    "style-loader",
                    "css-loader",
                    "webfonts-loader"
                ]
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "less-loader",
                        options: {
                            javascriptEnabled: true,
                            paths: [
                                paths.node_modules
                            ]
                        }
                    }
                ]
            },
            {
                test: /\.(js|jsx|ts|tsx)$/,
                include: [paths.src, paths.enlab],
                use: [
                    {
                        loader: "babel-loader",
                        options: babelOptions
                    },

                    {
                        loader: "ts-loader",
                        options: {
                            happyPackMode: true

                        }
                    }
                ]
            },
            {
                test: /\.(graphql|gql)$/,
                use: ["graphql-tag/loader"]
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                use: ["file-loader"]
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    "file-loader",
                    {
                        loader: "image-webpack-loader",
                        options: {
                            bypassOnDebug: true
                        }
                    }
                ]
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: `${paths.src}/index.ejs`
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new CaseSensitivePlugin(),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true
        }),
        new CircularDependencyPlugin({
            exclude: /a\.js|node_modules|energylab-components-v2/,
            failOnError: true,
            allowAsyncCycles: false,
            cwd: process.cwd()
        })
    ],
    devServer: {
        contentBase: paths.content,
        hot: true,
        host: "0.0.0.0",
        port: env.port || 3000,
        disableHostCheck: true,
        historyApiFallback: true,
        stats: "minimal",
        proxy: {
            "/graphql": {
                target: "https://dev.energylabconnect.com/api",
                changeOrigin: true
            },
            "/api/baloise/graphql": {
                target: "https://dev.energylabconnect.com/api/baloise/graphql",
                changeOrigin: true
            }
        }
    }
});
