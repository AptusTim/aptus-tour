import { Configuration } from "webpack";

import { paths } from "./path";

export const baseConfig: Configuration = {
    target: "web",
    entry: {
        babelPolyfill: "@babel/polyfill",
        index: "./src/index.tsx"
    },
    externals: {
        environment: "environment"
    },
    resolve: {
        modules: ["node_modules"],
        extensions: [".js", ".jsx", ".ts", ".tsx", ".css"],
        alias: {
            enlab: "enlab2"
        }
    },
    output: {
        path: paths.output,
        filename: "js/[name].[hash].js",
        chunkFilename: "js/[name].[chunkhash].js",
        publicPath: paths.public
    }
};
