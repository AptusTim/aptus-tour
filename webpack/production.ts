import * as cleanWebpackPlugin from "clean-webpack-plugin";
import * as FaviconsWebpackPlugin from "favicons-webpack-plugin";
import * as ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import * as HtmlWebpackPlugin from "html-webpack-plugin";
import * as MiniCssExtractPlugin from "mini-css-extract-plugin";
import * as OptimizeCssAssetsPlugin from "optimize-css-assets-webpack-plugin";
import * as webpack from "webpack";
import { Configuration } from "webpack";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import { babelOptions } from "./helpers/babelOptions";
import { paths } from "./path";

// tslint:disable-next-line:max-func-body-length
export const config = (): Configuration => ({
    bail: true,
    mode: "production",
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true
                        }
                    }
                ]

            },
            {
                test: /\.font\.js/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    {
                        loader: "webfonts-loader"
                    }
                ]
            },
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true
                        }
                    },
                    {
                        loader: "less-loader",
                        options: {
                            javascriptEnabled: true,
                            paths: [
                                paths.node_modules
                            ]
                        }
                    }
                ]
            },

            {
                test: /\.(js|jsx|ts|tsx)$/,
                include: [paths.src, paths.enlab],
                use: [
                    {
                        loader: "babel-loader",
                        options: babelOptions
                    },
                    {
                        loader: "ts-loader",
                        options: {
                            happyPackMode: true

                        }
                    }
                ]
            },
            {
                test: /\.(graphql|gql)$/,
                use: ["graphql-tag/loader"]
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        outputPath: "assets/fonts/"
                    }
                }]
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            outputPath: "assets/"
                        }
                    },
                    {
                        loader: "image-webpack-loader"
                    }
                ]
            }
        ]
    },
    optimization: {
        minimize: true,
        splitChunks: {
            cacheGroups: {
                commons: {
                    chunks: "initial",
                    minChunks: 2,
                    maxInitialRequests: 5, // The default limit is too small to showcase the effect
                    minSize: 0 // This is example is too small to create commons chunks
                },
                components: {
                    test: /node_modules\/enlab2/,
                    chunks: "initial",
                    name: "components",
                    priority: 15,
                    reuseExistingChunk: false
                },
                vendor: {
                    reuseExistingChunk: false,
                    test: /node_modules/,
                    chunks: "initial",
                    name: "vendor",
                    priority: 10,
                    enforce: true
                }
            }
        }
    },

    plugins: [
        new BundleAnalyzerPlugin({
            analyzerMode: "static"
        }),
        new webpack.HashedModuleIdsPlugin(),
        new cleanWebpackPlugin([paths.output], { allowExternal: true }),
        new HtmlWebpackPlugin({
            inject: true,
            template: `${paths.src}/index.ejs`,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true
            },
            hash: true
        }),
        new FaviconsWebpackPlugin(`${paths.src}/assets/favicon.png`),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].[contenthash].css",
            chunkFilename: "[id].[contenthash].css"
        }),
        new OptimizeCssAssetsPlugin(),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true
        })
    ]
});
