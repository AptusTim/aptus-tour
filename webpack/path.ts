const resolve = require("path").resolve;

import * as fs from "fs";

export const paths = {
    output: resolve(__dirname, "../", "build"),
    src: resolve(__dirname, "../", "src"),
    content: resolve(__dirname, "../", "public"),
    node_modules: resolve(__dirname, "../", "node_modules"),
    enlab: resolve(__dirname, "../", "node_modules/enlab2"),
    public: "/"
};
