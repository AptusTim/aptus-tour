import * as transformModulesPlugin from "babel-plugin-transform-modules";
import * as path from "path";
import { camelCaseToDash } from "./camelCaseToDash";

export const babelOptions = {
    presets: [
        ["@babel/env", { modules: false }]
    ],
    plugins: [
        "syntax-dynamic-import",
        ["@babel/plugin-proposal-decorators", { legacy: true }],
        [transformModulesPlugin, {
            antd: {
                transform: (importName: string, styleName: string) => {

                    const folder = camelCaseToDash(importName);

                    if (styleName) {
                        return `antd/es/${folder}/style/css.js`;
                    }

                    return `antd/es/${folder}`;
                },
                style: "css",
                preventFullImport: true
            }
        }]
    ]
};
